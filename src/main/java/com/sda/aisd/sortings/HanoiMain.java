package com.sda.aisd.sortings;

public class HanoiMain {

    public static void main(String[] args) {
        hanoi(20, 1, 2, 3);
    }

    private static void hanoi(int ilosc, int from, int posredni, int to) {
        if (ilosc == 1) {
            System.out.println(from + " -> " + to);
            return;
        } else {
            // z pacholka 'from'  do pacholka 'posredni', poslugujac sie pacholkiem 'to' jako posrednim
            hanoi(ilosc - 1, from, to, posredni);
            System.out.println(from + " -> " + to);
            // z pacholka 'posredni'  do pacholka 'to', poslugujac sie pacholkiem 'from' jako posrednim
            hanoi(ilosc - 1, posredni, from, to);
        }
    }
}
