package com.sda.aisd.sortings;

public class HanoiTowers {

    public static void main(String[] args) {
//        metoda(5);

        String tekst = "abecadlo";
//        char litera = tekst.charAt(tekst.length() - 1); // o
//        System.out.println(litera);
//        System.out.println(tekst.substring(0, tekst.length() - 1)); // wszystkie litery do 'o'
        System.out.println(odwrocString(tekst));
    }

    public static int silnia(int n) {
        if (n > 1) {
            return n * silnia(n - 1);
        } else {
            return 1;
        }
    }
    // odwrócenie stringa rekurencyjnie

    //
    public static String odwrocString(String doodwrocenia) {
        if (doodwrocenia.length() == 1) { // jesli jedna litera, to
            return doodwrocenia; // zwroc ta jedna litere
        } else {
            // ucinam jedna litere
            char ostatniaLitera = doodwrocenia.charAt(doodwrocenia.length() - 1);
            String subString = doodwrocenia.substring(0, doodwrocenia.length() - 1);
            return ostatniaLitera + odwrocString(subString);
        }
    }

    public static void metoda(int i) {
        if (i > 0) {
            System.out.println(i);
            metoda(i - 1);
            System.out.println(i);
        }
    }
}
