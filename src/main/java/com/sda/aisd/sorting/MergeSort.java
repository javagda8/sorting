package com.sda.aisd.sorting;

public class MergeSort {
    private static int counter = 0;

    public static int[] sort(int[] tab) {
        mergeSort(tab, 0, tab.length - 1, 0);

        System.out.println(counter);
        return tab;
    }

    public static void mergeSort(int[] tab, int from, int to, int level) {
        if (from == to) {
//            for (int i = 0; i < level; i++) {
//                System.out.print("-");
//            }
//            System.out.print("->");
//            System.out.print("Robię powrót na elemencie:" + tab[from]);
//            System.out.println();
            return;
        }
        for (int i = 0; i < level; i++) {
            System.out.print("-");
        }
        System.out.print("->");
        System.out.print("Rozpatruje tablicę:");
        System.out.println();
        print(tab, from, to, level);

        // int[] tab = new tab[] {1, 2, 3, 4, 5, 6, 7, 8};
        // from = 5, to = 8
        // (7-4) / 2 = 1 + 4 = 5
        int middle = ((to - from) / 2) + from;

//        for (int i = 0; i < level; i++) {
//            System.out.print("-");
//        }
//        System.out.print("->");
//        System.out.print("Dzielę lewą stronę tablicy:");
//        System.out.println();
//        print(tab, from, middle, level);
        mergeSort(tab, from, middle, level + 1);           // wykonanie podziału na lewej stronie

//        for (int i = 0; i < level; i++) {
//            System.out.print("-");
//        }
//        System.out.print("->");
//        System.out.print("Dzielę prawą stronę tablicy:");
//        System.out.println();
//        print(tab, middle + 1, to, level);
        mergeSort(tab, middle + 1, to, level + 1);   // wykonanie podziału na prawej stronies

        merge(tab, from, middle, to);
    }

    private static void print(int[] tab, int from, int to, int level) {
        for (int i = 0; i < level; i++) {
            System.out.print("-");
        }
        System.out.print("->");

        for (int i = from; i <= to; i++) {
            System.out.print(tab[i] + ", ");
        }
        System.out.println();
    }


    private static void merge(int[] tab, int from, int middle, int to) {

        int[] copy = new int[tab.length];
        for (int i = 0; i < tab.length; i++) {
            copy[i] = tab[i];
        }

        int indexLeft = from;
        int indexRight = middle + 1;
        int gdzieWstawiam = from; // to

        while ((indexLeft <= middle) && (indexRight <= to)) {
            counter++;
            if (copy[indexLeft] < copy[indexRight]) {
                tab[gdzieWstawiam] = copy[indexLeft];
                indexLeft++;
            } else {
                tab[gdzieWstawiam] = copy[indexRight];
                indexRight++;
            }
            gdzieWstawiam++;
        }

        while (indexLeft <= middle) {
//            counter++;
            tab[gdzieWstawiam++] = copy[indexLeft++];
        }
        while (indexRight <= to) {
//            counter++;
            tab[gdzieWstawiam++] = copy[indexRight++];
        }
    }
}
