package com.sda.aisd.sorting;

public class CountingSort {
    public static int[] sort(int[] tab, int zakres) {
        // 1. określ zakres
        // 1a. n = tab.length
        int n = tab.length;
        // 2. stwórz tablicę liczniki o rozmiarze 'zakres'
        int[] liczniki = new int[zakres + 1];
        // 2a. wyzeruj tablicę liczniki

        // 3. for i=0 i<n
        for (int i = 0; i < n; i++) {
            // 4.       liczniki[tab[i]] ++
            liczniki[tab[i]]++;
        }
        // 5. j =0;
        int j = 0;
        // 6. for i=0; i<zakres
        for (int i = 0; i < zakres + 1; i++) {
            // 7.       for k=0; k<liczniki[i]
            for (int k = 0; k < liczniki[i]; k++) {
                // 8.           tab[j++] = i;
                tab[j++] = i;
            }
        }
        // posortowane
        return tab;
    }
}
