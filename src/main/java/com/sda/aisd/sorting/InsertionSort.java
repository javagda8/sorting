package com.sda.aisd.sorting;

public class InsertionSort {
    public static int[] sort(int[] array) {
        int n = array.length;
        int counter = 0;

        for (int i = 1; i < n; i++) {

            int klucz = array[i];
            int j = i - 1;
            while (j >= 0) {
                counter++;
                if (array[j] > klucz) {
                    array[j + 1] = array[j];
                    j--;
                } else {
                    break;
                }
            }
            array[j + 1] = klucz;
        }
        System.out.println(counter);
        return array;
    }
}
