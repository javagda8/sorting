package com.sda.aisd.sorting;

public class BubbleSort {
    // przyjmujemy nie posortowaną tablicę,
    // zwracamy posortowaną
    public static int[] sort(int[] array) {
        // bubble sort...
        int counter = 0;
        int n = array.length;
        // for j in range(0,n):
        for (int j = 0; j < n; j++) {
            // for i in range(0,n-1):
            for (int i = 0; i < n - 1 - j; i++) {
                //if(A[i] > A[i+1]):
                counter++; //
                if (array[i] > array[i + 1]) {
                    //A[i],A[i+1] = A[i+1],A[i]
                    // zmiana wartosci
                    int tmp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = tmp;
                }
            }
        }
        System.out.println("Licznik: " + counter);
        return array;
    }
}
