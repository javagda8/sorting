package com.sda.aisd.sorting;

public class QuickSort {
    private static int counter = 0;

    public static int[] sort(int[] tab) {
        quick(tab, 0, tab.length - 1);

        System.out.println(counter);
        return tab;
    }

    private static void quick(int[] tab, int from, int to) {

        int pivot = tab[from]; // punkt względem którego odwracamy elementy
//        int pivot = tab[(to-from) /2 + from]; // punkt względem którego odwracamy elementy
//        int pivot = tab[(to+from) /2 ]; // punkt względem którego odwracamy elementy

        int i = from;
        int j = to;

        do {
            while (tab[i] > pivot) {
                counter++;
                i++;
            }

            while (pivot > tab[j]) {
                counter++;
                j--;
            }
            // jeśli wyszliśmy z obu pętli (powyższych)
            // to oznacza ze znalezlismy element wiekszy po lewej,
            // i mniejszy po prawej
            if (i <= j) {
                int tmp = tab[i];
                tab[i] = tab[j];
                tab[j] = tmp;
                i++;
                j--;
            }
        } while (i <= j);

        if (from < j) { // jesli mamy elementy z lewej (jesli from == j to 1 el)
            quick(tab, from, j);
        }
        if (i < to) { // jesli mamy elementy z prawej (jesli to == i to 1 el)
            quick(tab, i, to);
        }
        ///
    }
}
